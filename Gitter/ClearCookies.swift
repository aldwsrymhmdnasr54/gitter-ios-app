import Foundation
import WebKit

func ClearCookies(_ completionHandler: @escaping () -> Void) {
    URLCache.shared.removeAllCachedResponses()

    let date = Date(timeIntervalSince1970: 0)
    WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: date, completionHandler: completionHandler)
}
