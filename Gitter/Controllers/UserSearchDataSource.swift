import Foundation
import CoreData

class UserSearchDataSource: NSObject, UITableViewDataSource {

    var delegate: UserSearchDataSourceDelegate?

    private let restService = RestService()
    private let authSession = Api().authSession()
    private let context = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var results: [User] = [] {
        didSet {
            delegate?.userSearchResultsDidChange(self)
        }
    }

    private var previousQuery = ""

    func search(_ query: String) {
        guard query != previousQuery else {
            return
        }

        guard query.count > 0 else {
            results = []
            previousQuery = query
            return
        }

        // query db as it should be faster than network.
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "oneToOneRoom != NULL AND (displayName CONTAINS[cd] %@ OR username CONTAINS[cd] %@)", query, query)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "oneToOneRoom", ascending: false)]
        asyncFetch(fetchRequest) { (users) in
            if (query == self.previousQuery) {
                self.results = users
            }
        }

        NetworkIndicator.sharedInstance.visible = true
        restService.searchUsers(query) { (error, ids) in
            NetworkIndicator.sharedInstance.visible = false
            guard let ids = ids , error == nil else {
                return LogError("failed to search users via network", error: error!)
            }

            let fetchRequest = NSFetchRequest<User>(entityName: "User")
            fetchRequest.predicate = NSPredicate(format: "id IN %@ OR (oneToOneRoom != NULL AND (displayName CONTAINS[cd] %@  OR username CONTAINS[cd] %@))", ids, query, query)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "oneToOneRoom", ascending: false)]
            self.asyncFetch(fetchRequest) { (users) in
                if (query == self.previousQuery) {
                    self.results = users
                }
            }
        }

        previousQuery = query
    }

    func isEmpty() -> Bool {
        return results.isEmpty
    }

    func getItemAtIndex(_ indexPath: IndexPath) -> User {
        return results[(indexPath as NSIndexPath).row]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        let user = getItemAtIndex(indexPath)

        AvatarUtils.sharedInstance.configure(cell.avatar, avatarUrl: user.avatarUrl, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = user.displayName
        cell.sideLabel.text = "@" + user.username
        cell.bottomLabel.text = nil

        cell.badgeType = .none
        cell.badgeLabel.text = nil

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }

    private func asyncFetch(_ fetchRequest: NSFetchRequest<User>, completionHandler: @escaping ([User]) -> Void) {
        try! CoreDataSingleton.sharedInstance.uiManagedObjectContext.execute(NSAsynchronousFetchRequest(fetchRequest: fetchRequest, completionBlock: { (result) in
            let users = result.finalResult!
            DispatchQueue.main.async(execute: {
                completionHandler(users)
            })
        }))
    }
}

protocol UserSearchDataSourceDelegate {
    func userSearchResultsDidChange(_ roomSearchDataSource: UserSearchDataSource)
}
