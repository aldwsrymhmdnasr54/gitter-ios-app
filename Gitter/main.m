//
//  main.m
//  Troupe
//
//  Created by Andrew Newdigate on 04/02/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Gitter-Swift.h"

int main(int argc, char *argv[])
{    
#if BETA
    lcl_configure_by_component(lcl_cAPP, lcl_vDebug);
#else
#if RELEASE
    lcl_configure_by_component(lcl_cAPP, lcl_vWarning);
#else
    lcl_configure_by_component(lcl_cAPP, lcl_vDebug);
#endif
#endif
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    
}
